<?php

use Faker\Generator as Faker;

$factory->define(\App\Students::class, function (Faker $faker) {
    return [

        'name'=>$faker->unique()->name,
        'email'=>$faker->email,
         'web'=>$faker->address

    ];
});
