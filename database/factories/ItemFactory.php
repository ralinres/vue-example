<?php

use Faker\Generator as Faker;

$factory->define(App\Item::class, function (Faker $faker) {
    return [

         'title_id'=>$faker->numberBetween(1,20),
         'name'=>$faker->text,
         'body'=>$faker->paragraphs,


    ];
});
