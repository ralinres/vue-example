<?php

use Faker\Generator as Faker;

$factory->define(\App\Product::class, function (Faker $faker) {
    return [

           'name'=>$faker->unique()->name,
           'price'=>$faker->numberBetween(10,800)

    ];
});
