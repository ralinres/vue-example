<?php

use Illuminate\Database\Seeder;

class StudentTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Students::class,50)->create();
    }
}
