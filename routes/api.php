<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function(){

    Route::get('/students/all','StudentsController@index');
    Route::get('students/search','StudentsController@search');
    Route::get('students/find/{id}','StudentsController@getStudent');
    Route::get('students/sort','StudentsController@sort');
    Route::post('students/store','StudentsController@store');
    Route::post('students/delete','StudentsController@delete');
    Route::post('students/update','StudentsController@update');


});

Route::get('title/all','TitleController@index');
Route::post('/title/get','TitleController@get_titleByName');
Route::post('title/create','TitleController@create');
Route::post('title/delete   ','TitleController@destroy');
Route::post('title/update' , 'TitleController@update');
Route::post('item/{title}/create','ItemController@create');


Route::post('/get-details', 'API\PassportController@getDetails');
Route::get('/all','ProductController@index');
Route::get('/home', function () {
    return view('vueApp');
});
Route::post('/register', 'API\PassportController@register');
Route::post('/login/user', 'API\PassportController@login');
Route::post('item/update', 'ItemController@update');
Route::post('image/store', 'ImageController@store');

Route::get('/tomajose','ProductController@for');