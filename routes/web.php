<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/login', function () {
    return view('vueApp');
});

Route::get('/list', function () {
    return view('vueApp');
});

Route::get('/edit', function () {
    return view('vueApp');
});
Route::get('/', function () {
    return view('welcome');

});

Route::get('/home', 'HomeController@index')->name('home');

*/

Route::get('/students/all','StudentsController@index');

Route::get('/{any}', 'API\PassportController@index')->where('any', '.*');