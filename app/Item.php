<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

    protected $fillable = [
        'name', 'body',
    ];

    public function title(){

        return $this->belongsTo(Title::class);

    }

    public function images(){

        return $this->hasMany(FileUpload::class);


    }



}
