<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TitleModelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \App\Title::observe(\App\Observers\TitleObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
