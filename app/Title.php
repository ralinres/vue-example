<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Title extends Model
{

     public function items(){

          return $this->hasMany(Item::class);

     }

    public function addItem($name,$body){

        $item = new Item();
        $item->body =$body;
        $item->name =$name;
        $item->title_id = $this->id;
        $item->save();


    }



}
