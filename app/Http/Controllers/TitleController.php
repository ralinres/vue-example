<?php

namespace App\Http\Controllers;

use App\Title;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Exception;

class TitleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tiltes = Title::all();

        $response = array();

        foreach ($tiltes as $tilte )
        {

          if(count($tilte->items) == 0){

        $obj = array('id'=>'title'.$tilte->id,'text'=> $tilte->title,'state'=> ['checked' => true],'real_id'=>$tilte->id,'edit'=>false);

          }

          else{

              $childrens = array();
               foreach ($tilte->items as $item){

                   $objChild = array('id'=>'item'.$item->id,'real_id'=>$item->id,'text'=> $item->name,'body'=>$item->body,'edit'=>false,'editBody'=>false,'state'=> ['checked' => true]);

                    array_push($childrens,$objChild);

               }

              $obj = array('id'=>'title'.$tilte->id,'text'=> $tilte->title,'children'=>$childrens,'state'=> ['checked' => true],'real_id'=>$tilte->id,'edit'=>false,'select'=>false);
          }


             array_push($response,$obj);

       }
        return response()->json($response);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ;
    }

    public function create()
    {
        DB::beginTransaction();
        try{
            $title = new Title;
            $title->title = request('title');
            $title->save();
            $title->items()->createMany(request('array'));
            DB::commit();
        }catch (Exception $ex){

            DB::rollback();
            return response()->json(['error' => $ex->getMessage()], 500);

        }



        return response($title,201);
    }


    public function get_titleByName(Request $request)    {


         $title = Title::where('title',$request->title)->first();


         return response()->json($title);
    }


    public function show(Title $title)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Title  $title
     * @return \Illuminate\Http\Response
     */
    public function edit(Title $title)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Title  $title
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $title = Title::find(request('real_id'));
        $title->title = request('text');
        $title->save();
       // $title->update(['title'=>request('text')]);
        return response()->json($title,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Title  $title
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
          $id = request('real_id');
          $title = Title::find($id);
          $title->items()->delete();
          $title->delete();

          return  response()->json('Title deleted',201);
    }
}
