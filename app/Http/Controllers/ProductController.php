<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index()
    {
        return Product::all();
    }


    public function for(){

       $time_start = round(microtime(true) * 1000);

        $numbers = array();
        $n = 1000000;
        do {
            // for ($i=0;$i<1000000;$i++){
            $number = mt_rand(1, 9);
            $numbers[] = $number;
            //}
            $n --;
        }while($n>0);

       $time_end = round(microtime(true) * 1000);
        echo $time_end - $time_start;

        return response()->json($numbers);


    }


}
