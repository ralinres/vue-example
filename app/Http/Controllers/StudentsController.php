<?php

namespace App\Http\Controllers;

use App\Students;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    public function index()
    {
        return Students::paginate(5);
    }

     public function getStudent($id){

            $student = Students::findOrFail($id);
            return $student;

     }

    public function store(){


        $value = new Students;

        $value->name = request('name');
        $value->email = request('email');
        $value->web = request('web');
        $value->save();

        return response()->json($value,201);

    }

    public function delete(){

        $id = request('id');
        $student = Students::findOrFail($id);
        $student->delete();

    }


    public function update(){

        $id = request('id');
        $student = Students::findOrFail($id);
        $student->name = request('name');
        $student->email = request('email');
        $student->web = request('web');
        $student->save();

    }

    public function search(Request $request)
    {
       $students = Students::where('name', $request->keywords)
                             ->orWhere('name','like','%'.$request->keywords.'%')
                             ->orWhere('email','like','%'.$request->keywords.'%')
                             ->get();
                return response()->json($students);


    }

    public function sort(Request $request)
    {
        return $students = Students::orderBy('id',$request->orderSort)->get();

       // return response()->json($students);
    }
}
