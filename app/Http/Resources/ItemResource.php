<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ItemResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[

            'id'=> $this->id,
            'id_title'=> $this->id_title,
            'name'=> $this->name,
            'body'=> $this->body,

        ];
    }
    public function withResponse($request, $response)
    {
        $response->header('Access-Control-Allow-Origin','*');
    }
}
