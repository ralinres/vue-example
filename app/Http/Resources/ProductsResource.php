<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProductsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[

             'id'=> $this->id,
             'name'=> $this->name,
             'price'=>$this->price
        ];
    }
    public function withResponse($request, $response)
    {
        $response->header('Access-Control-Allow-Origin','*');
    }
}
