<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class StudentsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[

            'id'=> $this->id,
            'name'=> $this->name,
            'email'=>$this->email,
             'web'=>$this->web
        ];
    }
    public function withResponse($request, $response)
    {
        $response->header('Access-Control-Allow-Origin','*');
    }
}
