<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TitleCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {


           return [
               'data' => $this->collection,
               'links' => [
                   'self' => 'link-value',
               ],
           ];

    }
    public function withResponse($request, $response)
    {
        $response->header('Access-Control-Allow-Origin','*');
    }

}
