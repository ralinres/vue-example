<?php

namespace App\Observers;
use App\Title;

class TitleObserver
{
    public function creating(Title $title)
    {
        $title->title = strtoupper($title->title);
    }
}