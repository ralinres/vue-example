<?php

namespace App\Console\Commands;

use App\Title;
use Illuminate\Console\Command;

class DeleteTitle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:title';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete random title every minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $title = Title::first();
        $title->items()->delete();
        $title->delete();
        $this->info('Title was deleted');

    }
}
