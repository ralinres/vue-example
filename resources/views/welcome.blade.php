<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel</title>

    <link href="{{asset('css/materialize.min.css')}}" rel="stylesheet">

</head>
<body>
<div class="container">
    <div id="app"></div>
</div>
<script src="{{asset('js/app.js')}}"> </script>
</body>
</html>
