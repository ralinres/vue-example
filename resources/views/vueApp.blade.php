<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Crud con Vue</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{asset('css/materialize.min.css')}}" rel="stylesheet">
</head>
<body>


  <section id="app">
      <div class="container">

          <h3>CRUD con Vue</h3>

      </div>


  </section>

   <script src="{{asset('js/app.js')}}"> </script>
</body>
</html>