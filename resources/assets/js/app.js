
require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
import VueAxios from  'vue-axios';
import axios from 'axios';
import VueAuth from '@websanova/vue-auth'
import VueSession from 'vue-session'

export const auth =  {
    headers: {Authorization:'Bearer ' + localStorage.getItem('token')}
}

window.Vue.use(VueRouter,VueAxios,axios,VueAuth,VueSession);
//axios.defaults.baseURL = 'http://127.0.0.1:8000/api';

let AppLayout = require('./components/App.vue');
const listStudents = Vue.component('listStudents',require('./components/listStudents.vue'))
const addStudent = Vue.component('createStudent',require('./components/createStudent.vue'))
const editStudent = Vue.component('editStudent',require('./components/editStudent.vue'))
const deleteStudent = Vue.component('deleteStudent',require('./components/deleteStudent.vue'))
const showStudent = Vue.component('createStudent',require('./components/showStudent.vue'))
const login = Vue.component('login',require('./components/Login.vue'))
const home = Vue.component('home',require('./components/home.vue'))
const register = Vue.component('register',require('./components/register.vue'))
const dashboard = Vue.component('dashboard',require('./components/Dashboard.vue'))
const help = Vue.component('help',require('./components/help.vue'))
const addTitle = Vue.component('addTitle',require('./components/AddTilte.vue'))
const DeleteTitle = Vue.component('DeleteTitle',require('./components/DeleteTitle.vue'))
const tarea = Vue.component('tarea',require('./components/tarea'))
const error = Vue.component('error',require('./components/error'))
/*
var VueScrollTo = require('vue-scrollto');
Vue.use(VueScrollTo, {
    container: "body",
    duration: 500,
    easing: "ease",
    offset: 0,
    force: true,
    cancelable: true,
    onStart: false,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
})*/


const routes =[

    {
      name:'home',
      path:'/',
      component:home
    },
    {
        name: 'register',
        path: '/register',
        component: register,
    },
    {
      name:'login',
      path:'/login',
      component:login,
    },
    {
        name: 'dashboard',
        path: '/dashboard',
        component: dashboard
    },

    {
        name: 'help',
        path: '/help',
        component: help
    },
    {
      name:'listStudents',
      path:'/list',
      component:listStudents,
      meta:auth
    },
    {
        name:'addStudent',
        path:'/add',
        component:addStudent
    } ,
    {
        name:'editStudent',
        path:'/edit/:id',
        component:editStudent
    }
    ,{
        name:'deleteStudent',
        path:'/delete',
        component:deleteStudent
    },
    {
        name:'showStudent',
        path:'/show/:id',
        component:showStudent
    },
    {
        name:'tarea',
        path:'/show/tarea',
        component:tarea
    },
    {
        name:'addTitle',
        path:'/addtitle',
        component:addTitle
    },
    {
        name:'DeleteTitle',
        path:'/DeleteTitle',
        component:DeleteTitle
    },
    {
        name:'error',
        path:'/error',
        component:error
    }

]


const router = new VueRouter({mode:'history',routes: routes })

Vue.router = router


AppLayout.router = Vue.router

new Vue(AppLayout).$mount('#app')
