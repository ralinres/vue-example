<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Crud con VUE</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <style>
        :root{

            --color-primary:#41B883;
            --color-secundary:#35495E;
            --bg-color:#727F80
        }

        .ModalWindow{

            position: fixed;
            z-index: 999;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color:rgba(0,0,0,.25);
            display: none;


        }

        .ModalWindow-container{

             margin: 5vh auto 0;
             width: 60%;
            background-color:beige;

        }

        .ModalWindow-heading{


            padding: 0 1rem;
            background-color: #727F80;
            color: #FFF;

        }

        .ModalWindow-content{

            padding: 1rem;

        }

        .btn,
        .btn-large{

            background-color:var(--color-primary);

        }
        .btn,
          .btn-large:hover{

            background-color:var(--color-secundary);

        }


        .fade-enter-active, .fade-leave-active {
            transition: opacity .8s;
        }
        .fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
            opacity: 0;
        }

        .u-show{

            display: initial;

        }
        .u-flexColumnCenter{

            padding: 1rem;
            display: flex;
            justify-content: center;
            align-items: center;

        }




    </style>
</head>

<main id="app" class="container center">


     <section class="row">

           <div class="col s12">

               <h4>Curso Vue desde 0</h4>

           </div>


     </section>

     <section class="row valign-wrapper">

         <div class="col s12">

              <h3 class="left">Lista de Estudiantes </h3>

         </div>

         <div class="col s10">

             <button class="btn-large btn-floating" @click="toggleModal('add')">

                 <i class="material-icons">add_circle</i>

             </button>

             <div class="input-field col s9">

                 <i class="material-icons prefix">search</i>
                 <input name="search" v-model="keywords" type="text" placeholder="Inicie la busqueda" >
             </div>


         </div>

     </section>
    <hr>
    <transition name="fade">

        <p class="u-flexColumnCenter red accent-1 red-text text-darken-4" v-if="errorMessage">

            {{ errorMessage }}
            <i class="material-icons prefix">error</i>
        </p>

        <p class="u-flexColumnCenter green accent-1 green-text text-darken-4"v-if="succesMessage">

            {{ succesMessage }}
            <i class="material-icons prefix">check_circle</i>
        </p>

    </transition>
     <transition name="fade">

          <table class="responsive-table striped">

              <tr>

                  <th>Id</th>
                  <th>Nombre <div class="col s2">

                          <button @click="sort('desc')">

                              <i class="material-icons">arrow_drop_up</i>

                          </button>
                          <button  @click="sort('asc')">

                              <i class="material-icons">arrow_drop_down</i>

                          </button> <div> </th>
                  <th>Email</th>
                  <th>Web</th>
                  <th>Editar</th>
                  <th>Borrar</th>

              </tr>

              <tr v-for="student in students" :key="student.id">

                  <td>{{student.id}}</td>
                  <td>{{student.name}}</td>
                  <td>{{student.email}}</td>
                  <td>{{student.web}}</td>
                  <td>
                      <button class="btn-large btn-floating" @click="getStudent('edit',student)">

                          <i class="material-icons">edit</i>

                      </button>
                  </td>

                  <td>
                      <button class="btn-large btn-floating"@click="getStudent('delete',student)">

                          <i class="material-icons">delete</i>

                      </button>
                  </td>
              </tr>

          </table>


     </transition>
     <transition name="fade">


         <section :class="['ModalWindow',displayAddModal]" v-if="showAddModal">

             <div class="ModalWindow-container">

             <header class="ModalWindow-heading">
               <div class="row valign-wrapper">

                   <div class="col s10">

                       <h4 class="left">Agregar Estudiantes</h4>

                   </div>

                   <div class="col s2">

                       <button class="btn btn-floating right" @click="toggleModal('add')">

                           <i class="material-icons">close</i>

                       </button>

                   </div>

               </div>



             </header>

                 <form class="ModalWindow-content row" @submit.prevent="createStudent">

                     <div class="input-field col s12">

                         <i class="material-icons prefix">account_circle</i>
                          <input name="name" type="text" placeholder="Nombre" required>
                     </div>

                     <div class="input-field col s12">

                         <i class="material-icons prefix">email</i>
                          <input name="email" type="email" placeholder="Correo" required>
                     </div>


                     <div class="input-field col s12">

                         <i class="material-icons prefix">web</i>
                          <input name="web" type="text" placeholder="Web" required>
                     </div>


                        <div class="input-field col s12">

                            <button class="btn-large btn-floating right" type="submit" >

                                <i class="material-icons">save</i>

                            </button>
                     </div>


                 </form>

             </div>

         </section>

     </transition>
     <transition name="fade">


         <section :class="['ModalWindow',displayEditModal]" v-if="showEditModal">

             <div class="ModalWindow-container">

             <header class="ModalWindow-heading">
               <div class="row valign-wrapper">

                   <div class="col s10">

                       <h4 class="left">Editar Estudiante</h4>

                   </div>

                   <div class="col s2">

                       <button class="btn btn-floating right" @click="toggleModal('edit')">

                           <i class="material-icons">close</i>

                       </button>


                   </div>

               </div>



             </header>

                 <form class="ModalWindow-content row" @submit.prevent="updateStudent">

                     <div class="input-field col s12">

                         <i class="material-icons prefix">account_circle</i>
                          <input v-model="activeStudent.name" name="name" type="text" placeholder="Nombre" required>
                     </div>

                     <div class="input-field col s12">

                         <i class="material-icons prefix">email</i>
                          <input v-model="activeStudent.email" name="email" type="email" placeholder="Correo" required>
                     </div>


                     <div class="input-field col s12">

                         <i class="material-icons prefix">web</i>
                          <input v-model="activeStudent.web" name="web" type="text" placeholder="Web" required>
                     </div>


                        <div class="input-field col s12">

                            <button class="btn-large btn-floating right" type="submit" >

                                <i class="material-icons">save</i>

                            </button>

                            <input v-model="activeStudent.id" name="id" type="hidden" required>
                     </div>


                 </form>

             </div>

         </section>

     </transition>
     <transition name="fade">


         <section :class="['ModalWindow',displayDeleteModal]" v-if="showDeleteModal">

             <div class="ModalWindow-container">

             <header class="ModalWindow-heading">
               <div class="row valign-wrapper">

                   <div class="col s10">

                       <h4 class="left">Eliminar Estudiante</h4>

                   </div>

                   <div class="col s2">

                       <button class="btn btn-floating right" @click="toggleModal('delete')">

                           <i class="material-icons">close</i>

                       </button>


                   </div>

               </div>



             </header>

                 <form class="ModalWindow-content row" @submit.prevent="deleteStudent">

                     <div class="input-field col s12">

                         <p class="flow-text center">Esta seguro que desea eliminar el estudiante:<b>{{activeStudent.name}}</b> ?</p>
                         <input v-model="activeStudent.id" name="id" type="hidden" required>

                     </div>

                        <div class="input-field col s4 offset-s4">

                            <button class="btn-large btn-floating left" type="submit" >

                                <i class="material-icons">check</i>

                            </button>
                            <button class="btn-large btn-floating right" type="button" @click="toggleModal('delete')" >

                                <i class="material-icons">close</i>

                            </button>


                     </div>


                 </form>

             </div>

         </section>

     </transition>

</main>

<script src="../node_modules/vue/dist/vue.js"></script>
<script src="../node_modules/axios/dist/axios.js"></script>
<script>

     const app = new Vue({

           el:"#app",
           data:{
               keywords:'',
               orderSort:'',
               showAddModal:false,
               showEditModal:false,
               showDeleteModal:false,
               errorMessage:'',
               succesMessage:'',
               students:[],
               activeStudent:{}
           },
           mounted(){

                this.getAllStudents()

           },
           watch:{

               keywords(afterKey,beforeKey){

                   this.Search()

               }

           }
           ,
            computed:{

               displayAddModal(){

                   return(this.showAddModal)? 'u-show':''

               },
               displayEditModal(){

                   return(this.showEditModal)? 'u-show':''

               },
               displayDeleteModal(){

                   return(this.showDeleteModal)? 'u-show':''
               },

            },
            methods:{


                   toggleModal(modal){

                       if(modal === 'add'){

                           this.showAddModal = !this.showAddModal

                       }
                       else if(modal === 'edit'){

                           this.showEditModal = !this.showEditModal

                       }else if(modal === 'delete'){

                           this.showDeleteModal = !this.showDeleteModal

                       }


                   },
                    setMessages(res,msg){

                        if(res){

                            this.succesMessage = msg
                            this.getAllStudents()
                        }
                        else {

                            this.errorMessage = msg

                        }

                        setTimeout(()=>{

                          this.errorMessage = false
                          this.succesMessage = false

                        },2000)


                   },
                   Search(){

                       axios.get('http://127.0.0.1/api/students/search',{params:{keywords:this.keywords}})
                           .then(res =>{

                               //console.log(res)
                               this.students = res.data

                           })



                   },

                sort(param){

                    axios.get('http://127.0.0.1/api/students/sort',{params:{orderSort:param}})
                        .then(res =>{

                            //console.log(res)
                            this.students = res.data

                        })


                },
                getAllStudents(){

                       axios.get('http://127.0.0.1/api/students/all')
                           .then(res =>{

                               console.log(res)
                               this.students = res.data

                           })


                   },
                   createStudent(e){

                        axios.post('http://127.0.0.1/api/students/store',new FormData(e.target))
                            .then(res =>{

                                this.toggleModal('add')
                                this.setMessages(true,"Estudiante creado con exito")

                            })
                            .catch(()=> {

                                this.setMessages(false,"Error creando estudiante")
                                this.toggleModal('add')
                            })

                   },
                   getStudent(action,student){

                        this.toggleModal(action)
                        this.activeStudent = student



                   },
                    updateStudent(e){

                        axios.post('http://127.0.0.1/api/students/update',new FormData(e.target))
                            .then(res =>{

                                this.toggleModal('edit')
                                this.setMessages(res)

                            })


                    },
                    deleteStudent(e){

                        axios.post('http://127.0.0.1/api/students/delete',new FormData(e.target))
                            .then(res =>{

                                this.toggleModal('delete')
                                this.setMessages(res)

                            })

                    }

            }

     })



</script>
<body>

</body>
</html>