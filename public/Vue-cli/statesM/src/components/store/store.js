import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({


   state :{

    todos : [

      {
        id:18,
        task:'Tarea 221',
        complete: true

      },
      {
        id:0,
        task:'Tarea 121',
        complete: false

      },
      {
        id:8,
        task:'Tarea 1',
        complete: true

      },
      {
        id:9,
        task:'Tarea 19',
        complete: false

      },
      {
        id:10,
        task:'Tarea 14',
        complete: false

      }

    ]

  },

   getters :{

    getTodos:  state => state.todos

  },

   mutations :{

    ADD_TODO:(state,payload)=>{

      const newTask = {

        id: payload.id,
        task:payload.task,
        complete:false

      }
      /*unshift agrega al inicio*/
      state.todos.unshift(newTask)

    },
    CHANGE_TODO:(state,payload)=>{
      /*=> viene siendo como un where*/
      let item = state.todos.find(todo => todo.id === payload)
      item.complete = !item.complete

    },
    DELETE_TODO:(state,payload) => {

      let index = state.todos.findIndex(todo => todo.id === payload)
      state.todos.splice(index,1)

    }


  },

   actions :{

    addTodo:(context,payload) => {

      context.commit('ADD_TODO',payload)

    },
    changetodo:(context,payload) => {

      context.commit('CHANGE_TODO',payload)

    },
    deletetodo:(context,payload) => {

      context.commit('DELETE_TODO',payload)

    }

  }


})
